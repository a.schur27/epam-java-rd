package epam.rd.concurrency;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorsExample {

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(5);
        List<Future<Integer>> results = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            Future<Integer> result = service.submit(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    return new Random().nextInt();
                }
            });
            results.add(result);
        }
        for (Future<Integer> result : results) {
            try {
                System.out.println("Result from future: " + result.get());
            } catch (InterruptedException e) {
                System.out.println("Was interrupted");
            } catch (ExecutionException e) {
                System.out.println("Smth went wrong");
            }
        }
        service.shutdown();
    }

}
