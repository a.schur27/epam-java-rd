package epam.rd.concurrency;


import java.util.stream.IntStream;

public class JoinThread {

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                long sum = IntStream.range(0, Integer.MAX_VALUE).asLongStream().sum();
                System.out.println("Result from thread: " + sum);
            }
        });

        System.out.println("Is thread alive before start: " + thread.isAlive());
        thread.start();
        System.out.println("Is thread alive after start: " + thread.isAlive());
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Is thread alive after join: " + thread.isAlive());
    }

}
