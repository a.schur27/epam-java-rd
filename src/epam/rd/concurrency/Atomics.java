package epam.rd.concurrency;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Atomics {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(200);
        for (int i = 0; i < 200; i++) {
            service.submit(new CounterTask());
        }
        service.shutdown();
        service.awaitTermination(5, TimeUnit.SECONDS);
        System.out.println("Result: " + AtomicCounter.counter.get());
    }
}

class AtomicCounter {
    static AtomicInteger counter = new AtomicInteger(0);
}

class CounterTask implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            AtomicCounter.counter.getAndIncrement();
        }
    }
}

